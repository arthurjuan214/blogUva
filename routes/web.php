<?php

use App\Http\Controllers\AuthController;
use App\Http\Controllers\CommentController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\PostController;
use App\Http\Controllers\ProfileController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [HomeController::class, 'index'] )->name('home');


Route::get('/register', [AuthController::class, 'getRegister'])->name('register.form');
Route::post('/register', [AuthController::class, 'postRegister'])->name('register.store');
Route::get('/login', [AuthController::class, 'getLogin'])->name('login');
Route::post('/login', [AuthController::class, 'authenticate'])->name('login.authenticate');
Route::get('/logout', [AuthController::class, 'logout'])->name('register.logout')->middleware("auth");

Route::get('/posts', [PostController::class, 'index'])->name('posts.list');
Route::get('/post/create', [PostController::class, 'getCreate'])->middleware('auth');
Route::post('/post/create', [PostController::class, 'create'])->middleware('auth')->name('post.create');
Route::get('/post/{uuid}', [PostController::class, 'getPost'])->name('post.get');
Route::get('/post/edit/{uuid}',[PostController::class, 'getEdit']);
Route::post('/post/edit/{uuid}',[PostController::class, 'edit'])->name('post.edit');
Route::get('/post/delete/{uuid}', [PostController::class, 'destroy'])->name('post.delete');
Route::post('/comments/{uuid}', [CommentController::class, 'makeComment'])->middleware('auth')->name('comment.make');
Route::get("/comments/{post_id}", [CommentController::class, 'getAll'])->name('comments.get');
Route::delete('/comments/', [CommentController::class, 'delete'])->middleware('auth')->name('comment.delete');


Route::name('profile.')->group( function (){
    Route::get('/profile/me', [ProfileController::class, 'index'])->name('me')->middleware('auth');
    Route::get('/profile/me/edit', [ProfileController::class, 'getEdit'])->name('me.edit')->middleware('auth');
    Route::post('/profile/me/edit', [ProfileController::class, 'edit'])->name('edit')->middleware('auth');

    Route::get('/profile/{uuid}', [ProfileController::class, 'userProfile'])->name('user');
});
