<?php

namespace App\Repositories;

use App\Models\Post;
use App\Repositories\Contracts\IPostRepository;

class PostRepository implements IPostRepository
{

    public function getAll()
    {
        return Post::all();
    }

    public function getById($id)
    {
//        Post::where('id', $id);
        return Post::find($id);
    }

    public function create($post)
    {
        return Post::create($post);
    }

    public function getBySlug($slug)
    {
        // TODO: Implement getBySlug() method.
    }

    public function getAuthor($id)
    {
        return Post::find($id)->author();
    }
    public function getComments($id){
        return Post::find($id)->comments;
    }

    public function destroy($id)
    {
        return Post::destroy($id);
    }


    public function getByTitle($search)
    {
        return Post::where('title','LIKE', '%'.$search.'%')->get();
    }
}
