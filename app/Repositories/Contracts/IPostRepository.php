<?php

namespace App\Repositories\Contracts;

interface IPostRepository
{
    public function getAll();
    public function getById($id);
    public function create($post);
    public function getBySlug($slug);
    public function getAuthor($id);
    public function destroy($id);
    public function getComments($id);
    public function getByTitle($search);
}
