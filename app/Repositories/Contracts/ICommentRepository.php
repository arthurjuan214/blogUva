<?php

namespace App\Repositories\Contracts;

interface ICommentRepository
{
    public function create($comment);
    public function getAll($id);
    public function getById($id);

}
