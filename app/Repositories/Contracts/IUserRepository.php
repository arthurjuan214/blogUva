<?php

namespace App\Repositories\Contracts;

interface IUserRepository
{
    public function getAll();
    public function getById($id);
    public function create($user);
    public function getByEmail($email);
    public function getUserPosts($id);
}
