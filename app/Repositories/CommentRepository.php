<?php

namespace App\Repositories;

use App\Models\Comment;
use App\Repositories\Contracts\ICommentRepository;

class CommentRepository implements  ICommentRepository
{

    public function create($comment)
    {
      return Comment::create($comment);
    }

    public function getAll($id)
    {
        return Comment::where('post_id', $id);
    }

    public function getById($id)
    {
       return Comment::find($id);
    }


    public function destroy($id)
    {
        return Comment::destroy($id);
    }
}
