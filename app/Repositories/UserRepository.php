<?php

namespace App\Repositories;

use App\Models\User;
use App\Repositories\Contracts\IUserRepository;
use Illuminate\Database\Eloquent\Collection;

class UserRepository implements IUserRepository
{

    public function getAll(): Collection
    {
        return User::all();
    }

    public function getById($id)
    {
        return User::find($id);
    }

    public function create($user)
    {
        return User::create($user);
    }

    public function getByEmail($email)
    {
        return User::where('email', $email);
    }
    public function getUserPosts($id)
    {
       return User::find($id)->posts;
    }
}
