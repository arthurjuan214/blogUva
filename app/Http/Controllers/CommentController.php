<?php

namespace App\Http\Controllers;

use App\Services\Contracts\ICommentService;
use App\Services\Contracts\IUserService;
use http\Env\Response;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class CommentController extends Controller
{
    private ICommentService $_commentService;
    private IUserService $_userService;
    public function __construct(ICommentService $commentService, IUserService $userService){
        $this->_commentService = $commentService;
        $this->_userService = $userService;
    }

    private function populateArrayCommments($comment){
        $user = $this->_userService->getById($comment['user_id']);
    }
    public function getAll($id){
        $comments = $this->_commentService->getAll($id);
        foreach ($comments as $comment){

            $comment['user'] = $this->_userService->getById($comment['user_id']);
        }

        $response = [
            'success'=>true,
            'data'=>$comments
        ];

        return response()->json($response, 200);
    }

    public function makeComment(Request $request, $id){
        $request->validate([
            "comment"=>"string|required"
        ]);

        $data = $request->only("comment");
        $data['content'] = $data['comment'];
        $data['user_id'] = Auth::user()->id;
        $data['post_id'] = $id;

        $comment = $this->_commentService->makeComment($data);

        $response = [
            'success'=>true,
            'data'=>$comment
        ];
        return response()->json($response, 201);
    }

    public function delete(Request $request){
        try{
            $request->validate([
                'comment_id'=>'required'
            ]);
            $user_id = Auth::user()->id;
            $id = $request->only('comment_id');
            $deleted = $this->_commentService->delete($id, $user_id);



            return response()->json([
                'success'=>true,
                'data'=>'Comentário apagado'
            ]);
        }catch (\Exception $e){
                return response()->json([
                    'success'=>false,
                    'data'=>'Erro ao apagar o comentário: '.$e
                ]);
        }

    }


}
