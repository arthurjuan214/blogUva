<?php

namespace App\Http\Controllers;

use App\Services\Contracts\IUserService;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Routing\Redirector;
use Illuminate\Support\Facades\Auth;

class AuthController extends Controller
{

    private IUserService $_userService;
    public function __construct(IUserService $userService){
        $this->_userService = $userService;
    }
    public function getRegister(): View|Factory|Redirector|RedirectResponse|Application
    {
        if(Auth::check()){
            return redirect('/');
        }
        return view('pages.auth.register');
    }
    public function postRegister(Request $request): Redirector|Application|RedirectResponse
    {
        $request->validate([
            "firstName"=>"required | string",
            "lastName"=>"required | string",
            "email"=>"required | email | unique:users",
            "password"=>"required"
        ]);
        $data = $request->only('firstName','lastName', 'email', 'password');
        if($request->hasFile('image')){
                $request->validate([
                    'image'=>'|image|mimes:png,jpg,jpeg|max:2048'
                ]);
                $filename = $request->image->getClientOriginalName();
                $imageName = time().'-'.str_replace(' ', '-', $filename);
                $data['image'] = $imageName;
                $request->image->move(public_path('images'),$imageName);
        }

        $user = $this->_userService->create($data);
        Auth::login($user, $remember = true);
        return redirect('/');
    }

    public function getLogin(): Factory|View|Application
    {
        return view('pages.auth.login');
    }

    public function authenticate(Request $request): Redirector|Application|RedirectResponse
    {
        $request->validate([
            "email"=>"required | email",
            "password"=>"required"
        ]);

        $creds = $request->only('email', 'password');

        if(Auth::attempt($creds)){
            $request->session()->regenerate();
            return redirect('/');
        }
        return back()->withErrors(['msg'=>'Email ou senha Incorrertos']);
    }

    public function logout(Request $request)  : Redirector|Application|RedirectResponse
    {

            Auth::logout();
            $request->session()->invalidate();
            $request->session()->regenerateToken();

            return redirect('/');
    }
}
