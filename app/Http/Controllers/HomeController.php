<?php

namespace App\Http\Controllers;

use App\Services\Contracts\IPostService;

class HomeController extends Controller
{

    private IPostService $_postService;

    public function __construct( IPostService $postService )
    {
        $this->_postService = $postService;
    }

    public function index(){
        $posts = $this->_postService->getAll();

        foreach ($posts as $post){
            $post['author'] = $post->author;
        }

        return view('pages.home.index', ['posts'=>$posts]);
    }
}
