<?php

namespace App\Http\Controllers;

use App\Services\Contracts\IPostService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class PostController extends Controller
{
    private IPostService $_postService;
    public function __construct(IPostService $postService){
        $this->_postService = $postService;
    }

    public function index(Request $request){
        $search  = $request->input('search');
        if($search) {
            $posts = $this->_postService->search($search);
        }else{
            $posts = $this->_postService->getAll();
        }

        foreach ($posts as $post){
            $post['author'] = $post->author;
        }

        return view('pages.posts.index', ['posts'=>$posts]);
    }

    public function getCreate(){
        return view('pages.posts.create');
    }

    public function create(Request $request){
        $request->validate([
            'title' => 'required | string',
            'subtitle'=>'string',
            'content'=> 'required | string'
        ]);



        $data = $request->only('title', 'subtitle', 'content');
        $data['banner'] = $this->setBanner($request);

        $data['user_id'] = Auth::user()->id;
        $post = $this->_postService->create($data);

        if ( ! $post){
            $temp = 'blabla';
        }
        return redirect("/post/$post->id");

    }


    public function getPost(string $id)
    {
        $post = $this->_postService->getById($id);
        $post['author'] = $post->author;
        return view('pages.posts.detail', ['post'=>$post]);
    }


    public function getEdit($id){
        $post = $this->_postService->getById($id);
        if( ! $this->verifyPostOwner($id)){
            return redirect('/')->withErrors(['msg'=>'Você não possui acesso a esse POST']);
        }
        return view('pages.posts.edit', ['post'=>$post]);
    }

    public function edit(Request $request, $id){

        if( ! $this->verifyPostOwner($id)){
            return redirect('/')->withErrors(['msg'=>'Você não possui acesso a esse POST']);
        }

        $request->validate([
            'title' => 'string',
            'subtitle'=>'string',
            'content'=>'string'
        ]);

        $data = $request->only('title', 'subtitle', 'content');

        $data['banner'] = $this->setBanner($request);

        $newPost = $this->_postService->edit($data ,$id);
        return redirect("/post/$newPost->id");
    }


    public function destroy($id){
        if( ! $this->verifyPostOwner($id)){
            return back()->withErrors(['msg'=>'Você não é o autor desse post!']);
        }

        $this->_postService->destroy($id);
        return redirect('/');
    }

    private function verifyPostOwner($id) : bool
    {
        $post = $this->_postService->getById($id);
        if(Auth::user()->id != $post->author->id){
            return false;
        }
        return true;
    }

    /**
     * @param Request $request
     */
    public function setBanner(Request $request): ?string
    {

        if ($request->hasFile('banner')) {
            $request->validate([
                'banner' => '|image|mimes:png,jpg,jpeg|max:2048'
            ]);
            $filename = $request->banner->getClientOriginalName();
            $imageName = time() . '-banner-' . str_replace(' ', '-', $filename);
            $request->banner->move(public_path('images'), $imageName);
            return $imageName;
        }
        return null;
    }


}
