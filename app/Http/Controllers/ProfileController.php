<?php

namespace App\Http\Controllers;

use App\Services\Contracts\IPostService;
use App\Services\Contracts\IUserService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ProfileController extends Controller
{
    private  IUserService $_userService;
    private IPostService $_postService;
    public function __construct(IUserService $userService, IPostService $postService){
        $this->_userService = $userService;
        $this->_postService = $postService;
    }

   public function index(){
        $id = Auth::user()->id;
        $posts = $this->_userService->getUserPosts($id);
        return view('pages.profile.me', ['posts' => $posts]);

   }

   public function userProfile($id){
        //busca o usuário pelo id
        $user = $this->_userService->getById($id);

        //busca os posts desse usuario
        $posts = $this->_userService->getUserPosts($id);
        //injeta os dados na view
        return view('pages.profile.user',['user'=>$user, 'posts'=>$posts]);
   }

   public function getEdit(){
       $user = $this->_userService->getById(Auth::user()->id);
       return view('pages.profile.edit', ['user'=>$user]);
   }

    public function edit(Request $request){
try {
    //valida os dados
    $request->validate([
        "firstName" => "  string",
        "lastName" => "  string",
        "email" => "  email",
    ]);
    $data = $request->only('firstName', 'lastName', 'email');
    if ($request->hasFile('image')) { //VERIFICA SE ALGUMA IMAGEM FOI ENVIADA
        $request->validate([
            'image' => '|image|mimes:png,jpg,jpeg|max:2048' //VALIDA A IMAGEM
        ]);
        $filename = $request->image->getClientOriginalName();
        $imageName = time() . '-' . str_replace(' ', '-', $filename); // GERA UM NOME ÚNICO PARA ELA
        $data['image'] = $imageName; //SALVA NO OBJETO DE DADOS
        $request->image->move(public_path('images'), $imageName); //SALVA A IMAGEM NA PUBLIC
    }
    $this->_userService->update($data, Auth::user()->id); //CHAMA O SERVIÇO DE UPDATE
    return redirect(route('profile.me'));
}catch (\Exception $e){
    dd($e);
}
    }

}
