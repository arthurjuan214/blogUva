<?php

namespace App\Services;

use App\Repositories\Contracts\IPostRepository;
use App\Services\Contracts\IPostService;
use Illuminate\Support\Str;

class PostService implements IPostService
{
    private IPostRepository $_postRepository;
    public function __construct(IPostRepository $postRepository){
        $this->_postRepository = $postRepository;
    }

    public function create($post)
    {
        $post['slug'] = Str::slug($post['title'], '-');
        return  $this->_postRepository->create($post);
    }
    public function edit($post, $id){
        $dbPost = $this->getById($id);
        $dbPost['title'] = $post['title'];
        $dbPost['subtitle'] = $post['subtitle'];
        $dbPost['content'] = $post['content'];

        if($post['banner']){
            $dbPost['banner'] = $post['banner'];
        }

        $dbPost->save();

        return $dbPost;

    }
    public function getById($id)
    {
        return $this->_postRepository->getById($id);
    }

    public function getAuthor($id)
    {
        return $this->_postRepository->getAuthor($id);
    }



    public function getAll()
    {
        return $this->_postRepository->getAll();
    }


    public function destroy($id)
    {
        return $this->_postRepository->destroy($id);
    }

    public function search($search)
    {
       return $this->_postRepository->getByTitle($search);
    }
}
