<?php

namespace App\Services\Contracts;

interface IPostService
{
    public function create($post);
    public function getById($id);
    public function getAuthor($id);
    public function edit($post, $id);
    public function getAll();
    public function destroy($id);
    public function search($search);
}
