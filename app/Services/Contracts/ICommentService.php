<?php

namespace App\Services\Contracts;

use GuzzleHttp\Psr7\Request;

interface ICommentService
{
    public function makeComment($comment);
    public function getAll($id);
    public function delete($id, $user_id);
}
