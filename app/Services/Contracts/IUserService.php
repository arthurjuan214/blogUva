<?php

namespace App\Services\Contracts;

interface IUserService
{
 public function create($user);
 public function getAll();
 public function getUserPosts($id);
 public function getById($id);
 public function update($data, $id);
}
