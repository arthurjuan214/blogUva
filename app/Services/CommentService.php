<?php

namespace App\Services;

use App\Repositories\Contracts\ICommentRepository;
use App\Repositories\Contracts\IPostRepository;
use App\Repositories\PostRepository;
use App\Services\Contracts\ICommentService;
use App\Services\Contracts\IPostService;

class CommentService implements ICommentService
{
    private ICommentRepository $_commentRepository;
    private IPostRepository $_postRepository;
    public function __construct(ICommentRepository $commentRepository, PostRepository $postRepository){

    $this->_commentRepository = $commentRepository;
    $this->_postRepository = $postRepository;
    }

    public function getById($id){
        return $this->_commentRepository->getById($id);
    }
    public function makeComment($comment)
    {
//        dd($comment);
        return $this->_commentRepository->create($comment);
    }

    public function getAll($id)
    {
        return $this->_postRepository->getComments($id);
    }

    public function delete($id, $user_id): bool|int
    {

        $comment = $this->getById($id);
        if($comment[0]['user_id'] != $user_id){

            return false;
        }
        $id = $comment[0]['id'];
        return $this->_commentRepository->destroy($id);
    }
}
