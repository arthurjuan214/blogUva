<?php

namespace App\Services;

use App\Repositories\Contracts\IUserRepository;
use App\Services\Contracts\IUserService;

class UserService implements IUserService
{
    private IUserRepository $_UserRepository;
    public function __construct( IUserRepository $userRepository ){
        $this->_UserRepository = $userRepository;
    }
    public function create($user)
    {
        return $this->_UserRepository->create($user);
    }

    public function getAll()
    {
        return $this->getAll();
    }

    public function getUserPosts($id){
        return $this->_UserRepository->getUserPosts($id);
    }



    public function update($user, $id){
        $dbUser = $this->getById($id); //gera um objeto de usuário pelo ID

        //SUBSTITUI AS INFORMAÇÕES PELAS NOVAS
        $dbUser['firstName'] = $user['firstName'];
        $dbUser['lastName'] = $user['lastName'];
        $dbUser['email'] = $user['email'];

        if(array_key_exists('image', $user)){
            $dbUser['image'] = $user['image'];
        }
        //SALVA NO BANCO
        $dbUser->save();

        return $dbUser;
    }

    public function getById($id)
    {
        return $this->_UserRepository->getById($id);
    }
}
