<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Concerns\HasUuids;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Support\Str;

/**
 * @method static create($post)
 * @method static find($id)
 * @method static where(string $string, $id)
 */
class Post extends Model
{
    use HasFactory;
    use HasUuids;

    protected $fillable = ['title', 'subtitle', 'content', 'user_id', 'slug', 'banner'];

    public function author() : BelongsTo{
        return $this->belongsTo(User::class, 'user_id','id');
    }

    public function comments(): HasMany{
        return $this->hasMany(Comment::class);
    }

    public function category() : BelongsTo{
        return $this->belongsTo(Category::class);
    }
}
