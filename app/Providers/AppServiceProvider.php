<?php

namespace App\Providers;

use App\Models\Comment;
use App\Repositories\CommentRepository;
use App\Repositories\Contracts\ICommentRepository;
use App\Repositories\Contracts\IUserRepository;
use App\Repositories\PostRepository;
use App\Repositories\UserRepository;
use App\Services\CommentService;
use App\Services\Contracts\ICommentService;
use App\Services\Contracts\IPostService;
use App\Services\Contracts\IUserService;
use App\Services\PostService;
use App\Services\UserService;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register(): void
    {
        $this->app->bind(IUserRepository::class, function (){
            return new UserRepository();
        });
        $this->app->bind(IUserService::class, function ($app){
            return new UserService($app->make(UserRepository::class));
        });

        $this->app->bind(IPostService::class, function ($app){
            return new PostService($app->make(PostRepository::class));
        });
        $this->app->bind(ICommentService::class, function ($app){
            return new CommentService($app->make(CommentRepository::class), $app->make(PostRepository::class));
        });
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
