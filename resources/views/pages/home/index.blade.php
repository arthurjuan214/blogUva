@extends('layout.default')

@section('content')
    @if($errors->any())
        <div class="alert alert-danger" role="alert">
            <h4>{{$errors->first()}}</h4>
        </div>
    @endif


        <div class="container">
            <div class="row">
                <div class="glider">
                    @foreach($posts as $post)
                        <div class="col-md">
                            <div class="card text-center border border-primary shadow-0 mr-5" style="margin-right: 5px;">
                                <div class="bg-image hover-overlay ripple" data-mdb-ripple-color="light" style="max-height: 200px;">
                                    <img src="{{asset('images/'.$post->banner)}}" class="img-fluid" />
                                    <a href="#!">
                                        <div class="mask" style="background-color: rgba(251, 251, 251, 0.15)"></div>
                                    </a>
                                </div>
                                <a href="/profile/{{$post->author->id}}"  class="card-header">{{$post->author->getFullName()}}</a>
                                <div class="card-body">
                                    <h5 class="card-title">{{$post->title}}</h5>
                                    <p class="card-text text-truncate">
                                        {{$post->content}}
                                    </p>

                                    <a href="/post/{{$post->id}}" type="button" class="btn btn-primary">Ver detalhe</a>
                                </div>
                                <div class="card-footer">{{$post->created_at->isoFormat('d/m/Y')}}</div>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>

        </div>

    <style>
        .card{
            height: 100%;
        }
    </style>

        <script>
            new Glider(document.querySelector('.glider'), {
                slidesToShow: 5,
                slidesToScroll: 5,
                draggable: true,
                dots: '.dots',
                arrows: {
                    prev: '.glider-prev',
                    next: '.glider-next'
                }
            });
        </script>

@stop
