@extends('layout.default')

@section('content')

    <div class="container" style="display: flex;">
        <div class="row">
            @foreach($posts as $post)
                <div class="col-md-3" style="margin-bottom: 16px; margin-right: 5px">
                    <div class="card text-center border border-primary shadow-0 " style="max-height: 450px; height: 100%;">
                        <div class="bg-image hover-overlay ripple" data-mdb-ripple-color="light">
                            <img src="{{asset('images/'.$post->banner)}}" class="img-fluid" />
                            <a href="#!">
                                <div class="mask" style="background-color: rgba(251, 251, 251, 0.15)"></div>
                            </a>
                        </div>
                        <a href="/profile/{{$post->author->id}}"  class="card-header">{{$post->author->getFullName()}}</a>
                        <div class="card-body">
                            <h5 class="card-title">{{$post->title}}</h5>
                            <p class="card-text text-truncate">
                                {{$post->content}}
                            </p>

                            <a href="/post/{{$post->id}}" type="button" class="btn btn-primary">Ver detalhe</a>
                            @auth
                                @if($post->author->id == \Illuminate\Support\Facades\Auth::User()->id)
                                     <a href="/post/edit/{{$post->id}}" type="button" class="btn btn-primary">Editar</a>

                                @endif
                            @endauth
                        </div>
                        <div class="card-footer">{{$post->created_at->isoFormat('d/m/Y')}}</div>
                    </div>
                </div>
            @endforeach
        </div>

    </div>

@stop
