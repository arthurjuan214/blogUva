@extends('layout.default')

@section('content')

    <meta name="csrf-token" content="{{ csrf_token() }}">
<div class="container">
<div class="content" style="border-bottom: 1px solid black;margin-bottom: 36px; padding: 16px 0;">
    <div
        class="bg-image" style="
    background-image: url('{{asset("images/".$post->banner)}}');
    height: 400px;"></div>

<h1>{{$post->title}}</h1>

    @if($post->subtitle)
        <h2>{{$post->subtitle}}</h2>
    @endif

    <a href="/profile/{{$post->author->id}}"><small>{{$post->author->getFullName()}}</small></a>
    <article>
        <div style="width: 100%">
        {{$post->content}}

        </div>
    </article>
</div>
    <div class="comments">
        <form  id="comment">
            @csrf
            <div class="form-outline mb-4">
                <input type="text" id="comment" class="form-control" name="comment" />
                <label class="form-label" for="comment">Comentário..</label>
            </div>

            <button class="btn btn-info">
                Fazer comentário
            </button>

        </form>
        <div class="list_comments" style="margin-top: 16px;">

        </div>
    </div>
</div>

<script>
    const commentForm = document.querySelector("#comment");
    console.log(commentForm);

    commentForm.addEventListener("submit", (event)=>{
        event.preventDefault();
        const commentValue = commentForm.elements['comment'].value;
        const token = commentForm.elements['_token'].value;
        const data = {
            comment: commentValue,
            _token: token
        }

        const url = `{{route('comment.make', $post->id)}}`;
         fetch(url, {
            method: 'POST',
            body: new URLSearchParams(data),
             headers: new Headers({
                 "X-CSRF-TOKEN": token
             })

        }).then( (response) => {
             let commentValue = commentForm.elements['comment'].value;
             commentValue = '';
             mountComments()
         } )
    })


    async function getPosts(){
        const url = '{{route('comments.get', $post->id)}}';


        const response = await fetch(url);
        return response.json();
    }

     function mountComments(){
         getPosts().then(response => response)
             .then(( response ) => {
                 console.log(response);
                 let list = document.querySelector(".list_comments");
                 list.innerHTML = '';
                 response.data.sort( (a, b) =>{
                     return a.created_at > b.created_at ? -1 :  a.created_at < b.created_at ? 1 : 0
                 } )
                 response.data.forEach( (data) =>{

                     console.log(data);
                     list.innerHTML += `
                 <div class="row d-flex justify-content-center">
                <div class="col-md-8 col-lg-6">
                    <div class="card shadow-0 border" style="background-color: #f0f2f5;">
                        <div class="card-body p-4">


                            <div class="card mb-4">
                                <div class="card-body">
                                    <p>${data.content}</p>

                                    <div class="d-flex justify-content-between" style="margin-bottom:16px;">
                                        <div class="d-flex flex-row align-items-center">
                                          <a href="/profile/${data.user.id}">

                                            <img src="/images/${data.user.image}"
                                                 height="25" />
                                            <p class="small mb-0 ms-2">${data.user.firstName} ${data.user.lastName}</p>
                                           </a>
                                        </div>
                                    </div>
                                 @auth
                                    ${data.user_id == '{{\Illuminate\Support\Facades\Auth::user()->id}}' ? '<buttom class="btn btn-danger delete-comment" data-id=' + data.id +' onclick="deleteComment(event)"> Apagar </buttom>' : ''}
                                 @endauth
                                </div>
                            </div>



                        </div>
                    </div>
                </div>
            </div>
                `;
                 } )

             });

         makeDeleteFn();
    }

    mountComments();


    function deleteComment(event){
        const token = commentForm.elements['_token'].value;
        const url = `{{route('comment.delete')}}`
        const comment_id_data = event.target.dataset.id

        const data = {
            comment_id: comment_id_data,
            _token: token
        }
        if(window.confirm("Deseja apagar o comentário?")){
            fetch(url, {
                method: 'DELETE',
                body: new URLSearchParams(data),
                headers: new Headers({
                    "X-CSRF-TOKEN": token
                })
            }).then(response => response.json())
                .then(response => console.log(response));
        }


        mountComments();
    }




</script>

@stop
