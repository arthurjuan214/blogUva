@extends('layout.default')

@section('content')

<div class="container">
    <form method="post" action="{{route('post.create')}}" enctype="multipart/form-data">
        <!-- Name input -->
        @csrf
        <div class="form-outline mb-4">
            <input type="text" id="title" class="form-control" name="title" />
            <label class="form-label" for="title">Título *</label>
        </div>

        <!-- Email input -->
        <div class="form-outline mb-4">
            <input type="text" id="subtitle" class="form-control" name="subtitle"/>
            <label class="form-label" for="subtitle">Subtítulo</label>
        </div>


        <div class="form-outline mb-4">
            <label class="form-label" for="bannner">Banner</label>
            <input type="file" class="form-control" name="banner" id="banner" />
        </div>

        <!-- Message input -->
        <div class="form-outline mb-4">
            <textarea class="form-control" id="contnet" rows="4" name="content"></textarea>
            <label class="form-label" for="contnet">Texto...*</label>
        </div>

        <!-- Checkbox -->
{{--        <div class="form-check d-flex justify-content-center mb-4">--}}
{{--            <input class="form-check-input me-2" type="checkbox" value="" id="form4Example4" checked />--}}
{{--            <label class="form-check-label" for="form4Example4">--}}
{{--                Send me a copy of this message--}}
{{--            </label>--}}
{{--        </div>--}}

        <!-- Submit button -->
        <button type="submit" class="btn btn-primary btn-block mb-4">Enviar</button>
    </form>
</div>

@stop
