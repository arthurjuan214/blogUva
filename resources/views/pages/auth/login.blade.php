@extends('layout.default')
@section('content')
    <div class="container">


        @if($errors->any())
            <div class="alert alert-danger" role="alert">
                <h4>{{$errors->first()}}</h4>
            </div>
        @endif

        <form method="post" action="{{route('login.authenticate')}}">
            @csrf
            <!-- Email input -->
            <div class="form-outline mb-4">
                <input type="email" id="email" class="form-control" name="email" />
                <label class="form-label" for="email">Email</label>
            </div>

            <!-- Password input -->
            <div class="form-outline mb-4">
                <input type="password" id="password" class="form-control" name="password" />
                <label class="form-label" for="password">Senha</label>
            </div>

            <!-- 2 column grid layout for inline styling -->
            <div class="row mb-4">
                <div class="col d-flex justify-content-center">
                    <!-- Checkbox -->
                    <div class="form-check">
                        <input class="form-check-input" type="checkbox" value="" id="form1Example3" checked />
                        <label class="form-check-label" for="form1Example3"> Remember me </label>
                    </div>
                </div>
            </div>
            <div class="row mb-4">
                <div class="col d-flex justify-content-center">
                    <!-- Checkbox -->
                    <div class="form-check">
                        <a href="{{route('register.form')}}" class="form-check-label" for="form1Example3"> Criar conta </a>
                    </div>
                </div>
            </div>

            <!-- Submit button -->
            <button type="submit" class="btn btn-primary btn-block">Criar conta</button>
        </form>
    </div>
@stop
