@extends('layout.default')
@section('content')
 <div class="container">
    <form method="post" action="{{route('register.store')}}" enctype="multipart/form-data">
        @csrf
     <!-- Email input -->
        <div class="form-outline mb-4">
            <input type="text" id="name" class="form-control" name="firstName" />
            <label class="form-label" for="name">Nome</label>
        </div>

        <div class="form-outline mb-4">
            <input type="text" id="lastName" class="form-control" name="lastName" />
            <label class="form-label" for="lastName">Sobrenome</label>
        </div>

     <div class="form-outline mb-4">
         <input type="email" id="email" class="form-control" name="email" />
         <label class="form-label" for="email">Email</label>
     </div>


        <div class="form-outline mb-4">
            <label class="form-label" for="avatar">Profile Image</label>
            <input type="file" class="form-control" name="image" id="avatar" />
        </div>


        <!-- Password input -->
     <div class="form-outline mb-4">
         <input type="password" id="password" class="form-control" name="password" />
         <label class="form-label" for="password">Senha</label>
     </div>

     <!-- 2 column grid layout for inline styling -->
     <div class="row mb-4">
         <div class="col d-flex justify-content-center">
             <!-- Checkbox -->
             <div class="form-check">
                 <input class="form-check-input" type="checkbox" value="" id="form1Example3" checked />
                 <label class="form-check-label" for="form1Example3"> Remember me </label>
             </div>
         </div>

{{--         <div class="col">--}}
{{--             <!-- Simple link -->--}}
{{--             <a href="#!">Forgot password?</a>--}}
{{--         </div>--}}
     </div>

     <!-- Submit button -->
     <button type="submit" class="btn btn-primary btn-block">Criar conta</button>
 </form>
 </div>
@stop
