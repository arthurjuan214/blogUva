@extends('layout.default')

@section('content')

    <section style="background-color: #eee;">
        <div class="container py-5">
            <div class="row">
                <div class="col">
                    <nav aria-label="breadcrumb" class="bg-light rounded-3 p-3 mb-4">
                        <ol class="breadcrumb mb-0">
                            <li class="breadcrumb-item"><a href="/">Home</a></li>
                            <li class="breadcrumb-item"><a href="javascript:void(0);">User</a></li>
                            <li class="breadcrumb-item active" aria-current="page">User Profile</li>
                        </ol>
                    </nav>
                </div>
            </div>

            <div class="row">
                <div class="col-lg-4">
                    <div class="card mb-4">
                        <div class="card-body text-center">
                            <img src="{{asset('images/'.$user->image)}}" alt="avatar"
                                 class="rounded-circle img-fluid" style="width: 150px;">
                            <h5 class="my-3">{{$user->getFullName()}}</h5>
{{--                            <p class="text-muted mb-1">Full Stack Developer</p>--}}
{{--                            <p class="text-muted mb-4">Bay Area, San Francisco, CA</p>--}}
                            <div class="d-flex justify-content-center mb-2">
{{--                                <button type="button" class="btn btn-primary">Follow</button>--}}
{{--                                <button type="button" class="btn btn-outline-primary ms-1">Message</button>--}}
                            </div>
                        </div>
                    </div>
                    {{--                    <div class="card mb-4 mb-lg-0">--}}
                    {{--                        <div class="card-body p-0">--}}
                    {{--                            <ul class="list-group list-group-flush rounded-3">--}}
                    {{--                                <li class="list-group-item d-flex justify-content-between align-items-center p-3">--}}
                    {{--                                    <i class="fas fa-globe fa-lg text-warning"></i>--}}
                    {{--                                    <p class="mb-0">https://mdbootstrap.com</p>--}}
                    {{--                                </li>--}}
                    {{--                                <li class="list-group-item d-flex justify-content-between align-items-center p-3">--}}
                    {{--                                    <i class="fab fa-github fa-lg" style="color: #333333;"></i>--}}
                    {{--                                    <p class="mb-0">mdbootstrap</p>--}}
                    {{--                                </li>--}}
                    {{--                                <li class="list-group-item d-flex justify-content-between align-items-center p-3">--}}
                    {{--                                    <i class="fab fa-twitter fa-lg" style="color: #55acee;"></i>--}}
                    {{--                                    <p class="mb-0">@mdbootstrap</p>--}}
                    {{--                                </li>--}}
                    {{--                                <li class="list-group-item d-flex justify-content-between align-items-center p-3">--}}
                    {{--                                    <i class="fab fa-instagram fa-lg" style="color: #ac2bac;"></i>--}}
                    {{--                                    <p class="mb-0">mdbootstrap</p>--}}
                    {{--                                </li>--}}
                    {{--                                <li class="list-group-item d-flex justify-content-between align-items-center p-3">--}}
                    {{--                                    <i class="fab fa-facebook-f fa-lg" style="color: #3b5998;"></i>--}}
                    {{--                                    <p class="mb-0">mdbootstrap</p>--}}
                    {{--                                </li>--}}
                    {{--                            </ul>--}}
                    {{--                        </div>--}}
                    {{--                    </div>--}}
                </div>
                <div class="col-lg-8">
                    <div class="card mb-4">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-sm-3">
                                    <p class="mb-0">Full Name</p>
                                </div>
                                <div class="col-sm-9">
                                    <p class="text-muted mb-0">{{$user->getFullName()}}</p>
                                </div>
                            </div>
                            <hr>
                            <div class="row">
                                <div class="col-sm-3">
                                    <p class="mb-0">Email</p>
                                </div>
                                <div class="col-sm-9">
                                    <p class="text-muted mb-0">{{$user->email}}</p>
                                </div>
                            </div>
                            {{--                            <hr>--}}
                            {{--                            <div class="row">--}}
                            {{--                                <div class="col-sm-3">--}}
                            {{--                                    <p class="mb-0">Phone</p>--}}
                            {{--                                </div>--}}
                            {{--                                <div class="col-sm-9">--}}
                            {{--                                    <p class="text-muted mb-0">(097) 234-5678</p>--}}
                            {{--                                </div>--}}
                            {{--                            </div>--}}
                            {{--                            <hr>--}}
                            {{--                            <div class="row">--}}
                            {{--                                <div class="col-sm-3">--}}
                            {{--                                    <p class="mb-0">Mobile</p>--}}
                            {{--                                </div>--}}
                            {{--                                <div class="col-sm-9">--}}
                            {{--                                    <p class="text-muted mb-0">(098) 765-4321</p>--}}
                            {{--                                </div>--}}
                            {{--                            </div>--}}
                            {{--                            <hr>--}}
                            {{--                            <div class="row">--}}
                            {{--                                <div class="col-sm-3">--}}
                            {{--                                    <p class="mb-0">Address</p>--}}
                            {{--                                </div>--}}
                            {{--                                <div class="col-sm-9">--}}
                            {{--                                    <p class="text-muted mb-0">Bay Area, San Francisco, CA</p>--}}
                            {{--                                </div>--}}
                            {{--                            </div>--}}
                        </div>
                    </div>
                    <div class="row">
                        @foreach($posts as $post)
                            <div class="col-md-4" style="margin-bottom: 16px;">
                                <div class="card text-center border border-primary shadow-0 ">
                                    <div class="bg-image hover-overlay ripple" data-mdb-ripple-color="light">
                                        <img src="{{asset('images/'.$post->banner)}}" class="img-fluid" />
                                        <a href="#!">
                                            <div class="mask" style="background-color: rgba(251, 251, 251, 0.15)"></div>
                                        </a>
                                    </div>
                                    <div class="card-header">{{$post->author->getFullName()}}</div>
                                    <div class="card-body">
                                        <h5 class="card-title">{{$post->title}}</h5>
                                        <p class="card-text text-truncate">
                                            {{$post->content}}
                                        </p>
                                        <div class="d-flex" style="gap: 5px; flex-wrap: wrap; justify-content: center">
                                            <a href="/post/{{$post->id}}" type="button" class="btn btn-primary">Ver detalhe</a>
                                            @auth
                                                @if($post->author->id == \Illuminate\Support\Facades\Auth::User()->id)
                                                    <a href="/post/edit/{{$post->id}}" type="button" class="btn btn-primary">Editar</a>

                                                @endif
                                            @endauth
                                        </div>
                                    </div>
                                    <div class="card-footer">{{$post->created_at->isoFormat('d/m/Y')}}</div>
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </section>

@stop
