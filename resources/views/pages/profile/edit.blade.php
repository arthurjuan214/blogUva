@extends('layout.default')
@section('content')
 <div class="container">
    <form method="post" action="{{route('profile.edit')}}" enctype="multipart/form-data">
        @csrf
     <!-- Email input -->
        <div class="form-outline mb-4">
            <input value="{{$user->firstName}}" type="text" id="name" class="form-control" name="firstName" />
            <label class="form-label" for="name">Nome</label>
        </div>

        <div class="form-outline mb-4">
            <input value="{{$user->lastName}}" type="text" id="lastName" class="form-control" name="lastName" />
            <label class="form-label" for="lastName">Sobrenome</label>
        </div>

     <div class="form-outline mb-4">
         <input value="{{$user->email}}" type="email" id="email" class="form-control" name="email" />
         <label class="form-label" for="email">Email</label>
     </div>


        <div class="form-outline mb-4">
            <label class="form-label" for="avatar">Profile Image</label>
            <input type="file" class="form-control" name="image" id="avatar" />
        </div>

     <!-- Submit button -->
     <button type="submit" class="btn btn-primary btn-block">Editar conta</button>
 </form>
 </div>
@stop
