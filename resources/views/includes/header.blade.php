<!-- Navbar -->
<nav class="navbar navbar-expand-lg navbar-light bg-light mb-5">
    <!-- Container wrapper -->
    <div class="container-fluid" style="justify-content: space-between;">
        <!-- Toggle button -->
        <button
            class="navbar-toggler"
            type="button"
            data-mdb-toggle="collapse"
            data-mdb-target="#navbarSupportedContent"
            aria-controls="navbarSupportedContent"
            aria-expanded="false"
            aria-label="Toggle navigation"
        >
            <i class="fas fa-bars"></i>
        </button>

        <!-- Collapsible wrapper -->
        <div class="collapse navbar-collapse" id="navbarSupportedContent" style="flex-grow: 0">
            <!-- Navbar brand -->
            <a class="navbar-brand mt-2 mt-lg-0" href="/">
                <img
                    src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAOEAAADhCAMAAAAJbSJIAAAAq1BMVEX/AHb///8ASnr/AG3/AHT/AHL/ytv/AGoATHr/AG//L4P/ZZz/XZj/6/H/AG7/5+7/o8L/8vb/wdX/cqT/0d//lLf/4uv/+fv/Q4v/rMf/f6v/I37dGXfCJnc8RXqvLHj1DHb/t87/a6BxPHnsEnf/2eX/l7r/udBOQnr/xNf/i7L/U5IqR3qPNXlZQHrKI3d+OXmcMXi5KHj/AGSMNnl0O3k2RXqlL3hiP3l5654gAAAE00lEQVR4nO3ZWUOjPBSHcVqgQO2qVccu2rrMjDouo47O9/9kL23JyWFp5cIM78Xzu6qhS/4mHELwPAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAEfmT4vF4un6X/xU4GcS3TowrYGX+PZ1XvmIbUm83eYP57Hx92L5lWkqBPeTztZKdWrwI2ucfPNXHXmdj5gcm49OLrdHBiN57/HOiIt1vLaRvr6au4mWCQ9amX5kW6Oxae35I/OydTbIfTSSA62sZWZbTsPq35ufq3gm5PPSSbYtm3CoE/YlYejZXke5T17KgVt/05Ic24Sto8qIv0v5NhnbTw0mDPyJ9PpS99rvSPv9dpL6Q5Vw4lf82lVVvk3GiwYTqrHq5HotzcNtc3Df0mbFwuR5P3cFTCP+bi6hl8hJ2dIfvJPWrKr4t7mEo1Kt2TmCm4gvzSUcnEmv72yvoxs7WllTLmDhrE197AuYRnRUUmskDLrS6xN5j2qsqjNrB/lac70/YDt+byyhHi65JCb2ItILC5/JdPKD+PhJQlfVpk7CcCq9lkviQN7Rz+pMr1U00z80/yxgqrGEXnBq/hxnJ6Kqm9+zOnNYSvhd15q9ZSYbRCfFplbCwUp6nZ1c/g9pMZ+RBnlzX11cljWGMP7ZWMLgm3TflBUZ1ewamcjFY2iz3ttL4qJGwnbcWEIvOskPWXjUyr9BrWdGr7IIOrSD+FYroYvFW72E6vI+XU/TSEKMt59S52U3sYXJfuOe5YxK6GJhUy+hl0int5dE+bNUZ9LjoRy1K4RfNQK24+fmEqpK2Q30xb20nkmXcCquTNN6Ca+aS6hmYbrejOSkK9WZ9cQM7aVRVgh1AjaaUFWScaRKq1nPyNHJ+lsiqbQrs0L4389SPTF7kSzF+9sAaoQ3hUit1c00Pa9VaR6cJtyxi2Gmmd2hOHyVo+X1TBKmEjvI02z5/adWQheLGptQ3+34Ms0kob2lP5WPyF2uNNwcHK1dyv/gJvvWi1oJXewuqglmF8rq1khKhf1XtKT72d1D6b5Jy76gzsLbzdJbhbF7R+qa3ZWFlz8u9f6gWGcqmLuR9xorbxenYTo2MiGHpioEdt9ibG8P1B2hOVhaz1Q4zb6hxjR1MknTobG3t7eDZJMvtE1qZRnMip3ffd+kmanxeUAXV0MvfxL1p7MwnN2p6ah3IuxiNGMO7A0o24ovn97jO3qKURiace5sO9EbEWpbcaO8nqlmTuVPtjHiDzcBczezJfknFXapsmFuhu2d1UhZ2dbsTFy29+4m/nUVMB3FnQGP8/vWyZk+WF7PHL4mlv13jM3Kbd8VIz53F9ALd9XCs8KOp7qytKrqTG7E1cqtZ07meeVTC+cB07Epb5NtIhS3dHXdlfWMnQG5k1b/O+zDgOtf1RFdlVERzjqtomGv/GhFbStW1Jlpfge4uPOx9VwxjHHb1Y6+Ffj3+YvayXRQ9XRMnbJmPWNrb+E5hdr50I8w5o/5jHEcPyydBdN998Pe6Pam3+8PJ6u7rl/9gDOYdY3snAukofysSQ51c83zt3ZsvX8svzzMLkHi+1HKH4TlB2PyJmNnw573ivnL29Xj4+Ofh8XyS7oOAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAOI/aPJJHh4tCaUAAAAASUVORK5CYII="
                    height="50"
                    alt="MDB Logo"
                    loading="lazy"
                />
            </a>
            <!-- Left links -->
            <ul class="navbar-nav me-auto mb-2 mb-lg-0">
                <li class="nav-item">
                    <a class="nav-link" href="{{route('posts.list')}}">Posts</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="{{route('post.create')}}">New Post</a>
                </li>
                @guest
                <li class="nav-item">
                    <a class="nav-link" href="{{route('login')}}">Login</a>
                </li>
                @endguest
            </ul>
            <!-- Left links -->
        </div>
        <div>
        <!-- Collapsible wrapper -->
        <form method="get" action="{{route('posts.list')}}" class="d-flex">
            <!-- Email input -->
            <div class="form-outline">
                <input type="text" id="search" class="form-control" name="search" />
                <label class="form-label" for="search">Busque por um post</label>
            </div>
            <button class="btn btn-info" >Buscar</button>
        </form>
        </div>
        <!-- Right elements -->
        <div class="d-flex align-items-center">
            <!-- Icon -->

            <!-- Notifications -->

            <!-- Avatar -->
            <div class="dropdown">
                <a
                    class="dropdown-toggle d-flex align-items-center hidden-arrow"
                    href="#"
                    id="navbarDropdownMenuAvatar"
                    role="button"
                    data-mdb-toggle="dropdown"
                    aria-expanded="false"
                >
                    @auth
                    <img
                        src="{{asset("images/".Auth::user()->image)}}"
                        class="rounded-circle"
                        height="25"
                        loading="lazy"
                    />
                    @endauth
                </a>
                <ul
                    class="dropdown-menu dropdown-menu-end"
                    aria-labelledby="navbarDropdownMenuAvatar"
                >
                    <li>
                        <a class="dropdown-item" href="{{route('profile.me')}}">My profile</a>
                    </li>
                    @auth
                        <li>
                            <div class="dropdown-item logout" style="cursor: pointer;" >Logout</div>
                        </li>
                    @endauth
                </ul>
            </div>
        </div>
        <!-- Right elements -->
    </div>
    <!-- Container wrapper -->
</nav>
<!-- Navbar -->
