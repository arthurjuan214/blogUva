<!-- Footer -->
<footer class="text-center text-lg-start bg-light text-muted">
    <!-- Section: Social media -->
    <section class="d-flex justify-content-center justify-content-lg-between p-4 border-bottom">

    </section>
    <!-- Section: Social media -->

    <!-- Section: Links  -->
    <section class="">
        <div class="container text-center text-md-start mt-5">
            <!-- Grid row -->
            <div class="row mt-3">
                <!-- Grid column -->
                <div class="col-md-3 col-lg-4 col-xl-3 mx-auto mb-4">
                    <!-- Content -->
                    <h6 class="text-uppercase fw-bold mb-4">
                        <i class="fas fa-gem me-3"></i>Blog UVA
                    </h6>
                    <p>
                        Aqui você pode postar e comentar sobre o que quiser!
                    </p>
                </div>
                <!-- Grid column -->

                <!-- Grid column -->
                <div class="col-md-2 col-lg-2 col-xl-2 mx-auto mb-4">
                    <!-- Links -->
                    <h6 class="text-uppercase fw-bold mb-4">
                        Ações
                    </h6>
                    <p>
                        <a href="/post/create" class="text-reset">Novo Post</a>
                    </p>
                    <p>
                        <a href="/posts" class="text-reset">Lista de posts</a>
                    </p>
                    <p>
                        <a href="/register" class="text-reset">Criar conta</a>
                    </p>
                    <p>
                        <a href="/login" class="text-reset">Login</a>
                    </p>
                </div>
                <!-- Grid column -->



            </div>
            <!-- Grid row -->
        </div>
    </section>
    <!-- Section: Links  -->

    <!-- Copyright -->
    <div class="text-center p-4" style="background-color: rgba(0, 0, 0, 0.05);">
        © 2022 Copyright:
        <a class="text-reset fw-bold" href="javascript:void(0);">Grupo da Sala da Dennis</a>
    </div>
    <!-- Copyright -->
</footer>
<!-- Footer -->
