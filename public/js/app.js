let btnLogout = document.querySelector(".logout");
btnLogout.addEventListener('click', handleLogout);

function handleLogout(){
   if(window.confirm("Deseja deslogar?")){
     window.location.href = '/logout';
   }
}
